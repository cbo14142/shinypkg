# package setup ----
usethis::create_package(path = "Avril/shinypkg")

# license ----
usethis::use_mit_license(name = "myname")

# readme
usethis::use_readme_rmd()
rmarkdown::render(input = "README.Rmd", output_format = "github_document")

# git ignore ----

# build ignore ----
usethis::use_build_ignore("dev_history.R")
usethis::use_build_ignore('README.html')
usethis::use_build_ignore('Dockerfile')

# dependies declaration ---
usethis::use_package("shiny")

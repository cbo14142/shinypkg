
<!-- README.md is generated from README.Rmd. Please edit that file -->

## Partie 1 : shinypkg, créer un package à partir d’un projet existant

<!-- badges: start -->

<!-- badges: end -->

L’objectif de ce tutorial est d’illustrer l’utilité d’un code R packagé
et de Git.

## Objectifs d’apprentissage

  - Base de Git : fork, clone, add, commit, push.
  - Utilisation de git : en ligne de commande depuis le terminal.
  - Base d’un package R shiny

## Travailler sur un projet existant

Ce projet s’appuie sur le matériel disponible en :
<https://gitlab.com/cbo14142/shinypkg>

### 0\) Si besoin :

  - Télécharger et installer Git : <https://git-scm.com/downloads>

  - Configurer RStudio pour utiliser Git, dans RStudio \> Tools \>
    Global Options \> Git/SVN. Si le chemin vers l’exécutable `git.exe`
    n’est pas renseigné vérifier qu’il est présent en “C:/Program
    Files/Git/bin/git.exe” et l’ajouter.

  - Configurer git, dans le terminal de RStudio taper et compléter les
    commandes à suivre :

<!-- end list -->

``` r
git config --global user.name 'Your Name'
git config --global user.email 'your@email.com'
```

  - Créer un compte sur Gitlab (<https://gitlab.com/>)

### 1\) Forker (créer une fourche) le projet

Utiliser le projet de base (<https://gitlab.com/cbo14142/shinypkg>).

> Cela va importer le projet (dossiers, scripts, …) dans votre espace
> personnel

### 2\) Cloner le projet (depuis votre espace)

> Permet d’importer le projet sur son PC local.

  - 2.1 Dans les options de **Clone** (en bleu) copier *l’adresse http
    du répertoire projet*.
  - 2.2 Puis dans RStudio, placez vous dans l’arborescence voulu (e.g :
    “\~/Document/Gitlab\_training”).
  - 2.3 Entrez dans le terminal de RStudio la commande `git clone` puis
    collez *l’adresse http du répertoire projet* et exécutez cette ligne
    (en tapant sur `Entrer`) comme ci-dessous :

<!-- end list -->

``` r
git clone https://gitlab.com/cbo14142/myawesomeproject.git
```

2.4 Taper `git branch -a` pour visualiser les différentes branches
locales et distantes.

### 3\) Collaborer au projet

L’objectif est de créer une application de base avec 3 scripts : `ui.R`,
`server.R` et `run_app.R`.

**Application backlog**

Cette application s’appuiera sur le jeux de donnée `iris` et
représentera un simple scatterplot avec les espèces comme variable de
coloration et en dessous un tableau de statistiques descriptives (min,
médiane, moyenne, max). L’utilisateur aura le choix des variables à
représenter en abscisse et ordonné.

  - 3.1 Créer l’UI `ui.R` avec la commande `usethis::use_r("ui.R")`.
    Toutes les fonctions intervenant dans le package seront stockées
    dans le dossier `R`.

  - 3.2 Compléter le script selon les indications ci-dessus.

  - 3.3.1 Vérifier à enregistrer dans le répertoire git avec `git
    status`

  - 3.3.2 Ajouter ce script au répertoire git avec les commandes :

<!-- end list -->

``` r
git add ui.R
git commit -m "UI ajouté au projet"
```

> “git add” indique à git qu’il y a un nouveau fichier (ou un changement
> dans un fichier) à prendre en compte “git commit” écrit dans le
> répertoire git les changements dans le(s) fichier(s) concerné(s) “git
> commit -m” prends pour option “-m” = “message”

> En pratique il est recommendé d’effectuer des commits réguliers avec
> des messages clairs et concis expliquant pourquoi le changement a été
> effectué

  - 3.3.3 Vérifier qu’il n’y a pas de nouveau changement à commiter

  - 3.5 Ecrirer une fonction `stat_fun` calculant les statiques
    descriptives dans `stat_fun.R` avec les étapes git précédentes

  - 3.5 Reprendre les étapes précédentes pour créer `server.R`

  - 3.6 Reprendre les étapes précédentes pour créer `run_app.R`

Cette fonction doit permettre de lancer l’application avec :

``` r
run_app()
```

  - 3.7 Pousser (push) les changements locaux vers le control de version
    distant :

<!-- end list -->

``` r
git push
```

### 4\) R Package

Un package permet de partager un ensemble de fonctions documentées, de
données, … en s’assurant que les librairies requises sont installées.

  - 4.1 Cliquez sur `shinypkg.Rproj`, dans la partie `Build Tools`
    assurez vous que l’onglet “Generate documentation with Roxygen” est
    cochée avec l’option (dans `Configure...`) `Build and restart`

  - 4.2 Dans l’onglet `Build` (au niveau de `Environment`, `History`,
    `Connections`) cliquez sur `Install and Restart` (ou `ctrl + shift +
    B`). Et voilà \!

  - 4.3.1 “Tester” la bonne installation du package en redémarrant la
    session R (Session \> Restart … ou `ctrl + F10`) et en exécutant :

<!-- end list -->

``` r
library(shinypkg)
run_app()

# OU
shinypkg::run_app()
```

  - 4.3.2 Vérifier la qualité du package en effectuant un “Check” (dans
    `Build`, à côté de `Install and Restart` ou avec `ctrl + shift + E`)

> L’objectif est d’obtenir un check parfait du package : 0 error, 0
> warning, 0 notes Cette étape est cruciale pour éviter les mauvaises
> supprises par la suite (erreur dans la dockerisation, dans
> l’intégration et le déploiement)

  - 4.3.3 Résoudre les messages du `check` :
    
      - 4.3.3.1 Pour le `warning` concernant la license, utiliser
        `usethis::use_mit_license`
      - 4.3.3.2 Pour les `note` concernant les fichiers non utiles à la
        compilation du package, ajouter ces fichiers au fichier
        `.Rbuildignore` avec une commande du type
        `usethis::use_build_ignore(files = "file_name")`.
      - 4.3.3.3 Résoudre tous les (éventuels) autres problèmes liés aux
        scripts R

  - 4.4.1 Documentez le code. Dans les scripts, placez vous dans les
    fonctions et insérez un squelette de documentation (dans Code \>
    Insert Roxygen Squeletton ou `ctrl + shift + alt + R`). Complétez
    les champs requis.

  - 4.4.2 Re-builder le package pour tester la documentation avec :

<!-- end list -->

``` r
?stat_fun()
```

Optionnel : ajouter un exemple à la documentation de `stat_fun`

  - 4.4.3 Commiter les changements

  - 4.5 Insérer un test pour le lancement de l’application avec :

<!-- end list -->

``` r
usethis::use_test(name = "test-run-app")
```

en y insérant le test :

``` r
testthat::expect_error(object = shinypkg::run_app(),
                       regexp = NA
                       )
```

Cette fonction, comme indiqué dans `?testthat::expect_error`, test
l’absence d’erreur dans le lancement de l’application.

Tester l’application dans Build \> More \> Test Package (ou `ctrl + alt
+ T`)

  - 4.6 Créer un test pour `stat_fun`

## Conclusions

Dans ce projet nous avons construit un package R versionné sur un
répertoire git local et distant avec Gitlab. Ce package peut être plus
à même d’être un travail collaboratif avec sa documentation (pour le
futur nous ou un collègue) et ses tests (qui détectent des changements
qui cassent le code).

**NB** : Nous avons utilisé git en ligne de commande car les
alternatives ne permettent pas toujours d’utiliser toutes les commandes
git (fusion de projet notamment). Il y a au moins 3 autre alternatives :

  - Ecrire script R d’historique de ses commandes avec la fonction
    `system` qui permet de passer des commandes depuis R au terminal.
    E.g : `system("git status")`.

  - Utiliser le package `git2r`. Plus d’informations :
    <https://docs.ropensci.org/git2r/> .

  - Utiliser le panel `Git` (que nous allons aussi utiliser par la
    suite)

Quelques ressources pour les commandes git :

  - la référence : <https://git-scm.com/docs/git-commit>
  - <https://www.git-tower.com/blog/git-cheat-sheet/>
  - <https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf>
  - <https://biologyguy.github.io/git-novice/reference/>

Ce projet partait d’une base existante, nous allons à présent voir
comment initialiser un projet de zéro. Suivez les instructions du
`README.md` du projet : …

## Partie 2 : mypkg, mon nouveau package sur git

## Objectifs

  - Git : initier un projet
  - Commandes git : remote add, push –set-upstream, branch, merge, pull

### 1\) Initier un projet git

  - 1.1 Sur Gitlab, créer un nouveau projet publique vide

  - 1.2 En local, créer un nouveau package R avec au choix :
    
      - Naviguer en clique-bouton dans la création d’un nouveau projet
        (avec d’un nouveau répertoire/dossier, un package R utilisant
        devtools)
    
      - Exécuter `usethis::create_package(path = "")` avec le chemin
        voulu dans le terminal (avec le nom du package `mypkg`)

  - 1.3 S’assurer que git est utilisé pour ce projet (cliquer sur le
    ficheir `mypkg.Rproj` et naviguer dans Git/SNV).

  - 1.4 S’assurer que `Roxygen` est utilisé pour compiler la
    documentation du package.

  - 1.5 S’assurer que la compilation du package se fait par installation
    et redémarage de la session R.

  - 1.6 Compiler le package vide.

  - 1.7 Ajouter une fonction `stat_fun` de calcul de statistiques
    descriptives au package.

  - 1.8 Compiler et faire un `check` du package.

  - 1.9 Versionner les changements effectués

<!-- end list -->

``` r
git add .
git commit -m ":baby: first commit"
```

  - 1.10 Pousser les changements vers le serveur Gitlab distant (aussi
    appelé remote) en adaptant l’adresse du serveur distant (`remote`).

> Ce serveur distant est (très) souvent appelé `origin`. Dans un premier
> temps nous déclarons ce nouveau serveur distant, le nommons et donnons
> sont adresse.

``` r
git remote add origin git@gitlab.com:cbo14142/mypkg.git
```

> Dans un deuxième temps nous poussons notre répertoire git et déclarons
> la synchronisation entre notre version du répertoire `master` et celle
> appelée aussi `master` sur le serveur distant `origin`

``` r
git push --set-upstream origin master
```

> Ces commandes sont documentées dans tous les projets vides sur Gitlab.
> E.g : <https://gitlab.com/cbo14142/mypkg>

### 2\) Travailler avec des branches

Nous allons à présent amener une évolution dans le projet avec
l’inclusion de données.

  - 2.1 Créer une nouvelle branche (une nouvelle version) du projet,
    elle est souvent appelée `dev` :

<!-- end list -->

``` r
git branch dev
```

  - 2.2 Vérifier la création de cette branche puis naviguer dans la
    branche dev avec :

<!-- end list -->

``` r
git checkout dev
```

  - 2.3 Ajouter un répertoire de données brutes `data-raw` avec :

<!-- end list -->

``` r
usethis::use_data_raw()
```

  - 2.4 Créer un fichier `mtcars2.R` dans `data-raw` qui créé le fichier
    `mtcars.csv` dans `data-raw` avec les noms de voitures comme
    variable et non nom de ligne.

  - 2.5 Ajouter ces données à l’utilisation du package

<!-- end list -->

``` r
?usethis::use_data
```

  - 2.6 Faire un commit puis un push de la nouvelle branche

> Suivre les instructions de git pour résoudre le problème

### 3\) Réunir les travaux avec `merge`

  - 3.1 Se positionner en `master`

  - 3.2 Effectuer un merge de `dev` dans `master`

<!-- end list -->

``` r
git merge dev
```

> Comme les changements ont été effectué uniquement en dev il n’y a pas
> de conflits C’est également le cas quand les travaux sont répartis
> entre les devs dans des branches séparées, sur des fichiers/dossiers
> distincts

### 4\) Gérer les conflits

  - 4.1 Créer un conflit en éditant `stat_fun.R` en master et dev avec :

  - master

<!-- end list -->

``` r
# un commentaire depuis master
```

  - dev

<!-- end list -->

``` r
# un commentaire depuis dev
```

  - 4.2 Commiter les changements. Faire un merge de `dev` dans `master`

  - 4.3 Régler les conflits

  - 4.4 Uniformiser les branches

  - 4.5 Régler les conflits avec Gitlab
    
      - 4.5.1 Créer 2 nouveaux conflits de la forme “\# un 2eme
        commentaire depuis master”
    
      - 4.5.2 Pousser ces changements sur `remote`
    
      - 4.5.3 Créer une “Merge request” pour merger `dev` en `master`
    
      - 4.5.4 Importer les changements de `remote` sur le répertoire git
        local

<!-- end list -->

``` r
git pull
```

  - 4.5.5 Vérifier et corriger le merge si besoin (avec commit et push)

  - 4.5.6 Uniformiser les branches

### 5\) Travailler avec les`Issues` Gitlab

Les “tickets” (`Issues`) des projets Gitlab permettent d’historiser
l’évolution d’un projet, les bugs et les nouvelles fonctions. E.g un
ticket nommé “Ajout d’un panel de visualisation de la production” peut
être affecté à un (des) développeur(s), avec un objectif de temps
déterminé, comme partie d’une évolution majeure du projet
(`Milestones`).

Créer des `Issues` permet également de rattacher des commits à un
objectif plus général. On peut notamment clore une issue avec le message
de commit tel que “closes \#1 fix the data source” où “closes \#1” est
le strict nécessaire pour clore l’`Issue` et le reste ce qui documente
le commit.

  - 5.1 Créer un ticket “Ajouter un README”

  - 5.2 Le résoudre
